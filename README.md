## COMUN Point System

COMUN, Colombo Model United Nations is a MUN Conference hosted anually in Colombo, Sri Lanka. 

This is a Ruby on Rails application developed to act as a faster means of recording points for the delegates that participated in this conference. 
This app would replace the previous paper system, thus increasing speed and integrity with the allocation of points to the delegates. 

Each commitee has its own page and an authorized user can login to make changes. The admin is capable of viewing the results of all the commitees.