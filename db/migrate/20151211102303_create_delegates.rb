class CreateDelegates < ActiveRecord::Migration
  def change
    create_table :delegates do |t|
      t.belongs_to :committee
      t.string :country

      t.integer :speaker_list_score, default: 0
      t.integer :speaker_list_count, default: 1

      t.integer :poi_score, default: 0
      t.integer :poi_count, default: 1

      t.integer :ror_score, default: 0
      t.integer :ror_count, default: 1

      t.integer :ros_score, default: 0
      t.integer :ros_count, default: 1

      t.integer :mod_structure_score, default: 0
      t.integer :mod_structure_count, default: 1

      t.integer :mod_speech_score, default: 0
      t.integer :mod_speech_count, default: 1

      t.integer :lobbying_score, default: 0
      t.integer :lobbying_count, default: 1

      t.integer :fps, default: 0

      t.integer :res_speech_score, default: 0
      t.integer :res_speech_count, default: 1

      t.integer :amendments_score, default: 0
      t.integer :amendments_count, default: 1

      t.integer :other_speeches_score, default: 0
      t.integer :other_speeches_count, default: 1

      t.boolean :sponsor, default: false

      t.timestamps null: false
    end
    add_index :delegates, :id
    add_index :delegates, [:country, :committee_id]
  end
end
