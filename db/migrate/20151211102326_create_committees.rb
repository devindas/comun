class CreateCommittees < ActiveRecord::Migration
  def change
    create_table :committees do |t|

      t.string :name
      t.boolean :special, default: false

      t.timestamps null: false
    end
    add_index :committees, :id
    add_index :committees, :name
  end
end
