# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

committees = %w(ga1 ga3 ga4)
committees2 = %w(ecosoc sc unhcr unep)

committees.each do |committee|
  Committee.create(name: committee)
end

committees2.each do |committee|
  Committee.create(name: committee, special: true)
end


ga1 = %w(Afghanistan Angola Algeria Argentina Bangladesh Belgium Bolivia Brazil Canada Chad Central_African_Republic China Colombia Cuba Czech_Republic Dominican_Republic DPRK DRC Ethiopia Ecuador Egypt Estonia France Germany Guatemala Greece Hungary India Iran Iraq Israel Japan Jordan Kenya Kuwait Lebanon Libya Malaysia Maldives Mexico Netherlands Nigeria Pakistan Peru Qatar RC ROK Russia Rwanda Saudi_Arabia Somalia Singapore South_Africa South_Sudan Sri_Lanka Sudan Syria Turkey UK Ukraine USA Venezuela Yemen Autonomous_region_of_Kurdistan Palestine Republic_of_China_Taiwan)

committee = Committee.find_by(name: 'ga1')
ga1.each do |country|
  Delegate.create(country: country, committee_id: committee.id)
end

ga3 = %w(Afghanistan Angola Argentina Australia Bangladesh Belarus Bolivia Brazil Cameroon Canada Chad Central_African_Republic Chile China Colombia Cuba Czech_Republic Djbouti Dominican_Republic DPRK DRC Ecuador Egypt Eritrea Estonia Finland France Germany Guatemala Greece India Indonesia Iran Iraq Israel Japan Jordan Kenya Lebanon Liberia Libya Malaysia Maldives Mexico Morocco Myanmar Nepal Netherlands Nigeria Pakistan Peru Phillipines Qatar RC ROK Russia Rwanda Saudi_Arabia Sierra_Leone Somalia Singapore South_Africa South_Sudan Sri_Lanka Sudan_ Switzerland Syria Thailand Turkey UK Ukraine USA Venezuela Vietnam Yemen Holy_See Autonomous_region_of_Kurdistan Sahrawi_Arab_Democratic_Republic Palestine Republic_of_China_Taiwan)

committee = Committee.find_by(name: 'ga3')
ga3.each do |country|
  Delegate.create(country: country, committee_id: committee.id)
end

ga4 = %w(Afghanistan Angola Algeria Australia Bahrain Bangladesh Belgium Brazil Cameroon Canada Chad Central_African_Republic Chile China Cuba Czech_Republic Djbouti DPRK DRC Ethiopia Egypt Estonia France Germany Greece India Iran Iraq Israel Japan Jordan Kuwait Lebanon Libya Morocco Nepal Nigeria Pakistan Qatar RC ROK Russia Rwanda Saudi_Arabia Singapore South_Africa South_Sudan Sri_Lanka Sudan Sweden Syria Turkey UK Ukraine USA Vietnam Yemen Autonomous_region_of_Kurdistan Sahrawi_Arab_Democratic_Republic Palestine)

committee = Committee.find_by(name: 'ga4')
ga4.each do |country|
  Delegate.create(country: country, committee_id: committee.id)
end

ecosoc = %w(Argentina Australia Austria Bangladesh Brazil Burkina_Faso China Colombia Croatia Democratic_Republic_of_the_Congo France Germany Ghana Greece Haiti India Italy Japan Kuwait Malaysia Nepal North_Korea Pakistan Philippines Portugal Republic_of_Korea Russia Saudi_Arabia Singapore South_Africa Spain Sudan Sweden Switzerland Trinidad_and_Tobago Uganda United_Kingdom_of_Great_Britain_and_Northern_Ireland United_States_of_America Vietnam Zimbabwe World_Bank NDB IMF AIIB ECB AfDB ADB IDB Bill_and_Melinda_Gates_Foundation Apple_Inc. Alphabet_Inc. Facebook Microsoft_Inc. Carrefour_S.A. Samsung ICBC Volkswagen_Group Emirates_Telecommunications_Corporation GlaxoSmithKline_Pharmaceuticals_Ltd. Sony_Corporation)

committee = Committee.find_by(name: 'ecosoc')
ecosoc.each do |country|
  Delegate.create(country: country, committee_id: committee.id)
end

sc = %w(China France India Japan Malaysia Philippines Russia Saudi_Arabia Singapore Spain United_Kingdom_of_Great_Britain_and_Northern_Ireland United_States_of_America Vietnam Angola Brunei New_Zealand Nigeria Taiwan Ukraine Venezuela)

committee = Committee.find_by(name: 'sc')
sc.each do |country|
  Delegate.create(country: country, committee_id: committee.id)
end

unhcr = %w(China France India Japan Russia United_Kingdom_of_Great_Britain_and_Northern_Ireland United_States_of_America New_Zealand Australia Brazil Germany Greece Italy Burundi Central_African_Republic Djibouti Egypt_ EU_Parliament_Representative Iran Iraq Israel Jordan Lebanon Libya Representative_of_Amnesty_International Representative_of_HR Representative_of_Kurdistan Representative_of_Palestine Somalia Syria Turkey United_Arab_Emirates Yemen)

committee = Committee.find_by(name: 'unhcr')
unhcr.each do |country|
  Delegate.create(country: country, committee_id: committee.id)
end

unep = %w(China France India Japan Russia United_Kingdom_of_Great_Britain_and_Northern_Ireland United_States_of_America Australia Brazil Germany Italy Egypt Philippines Saudi_Arabia Singapore Spain Nigeria Venezuela Argentina Burkina_Faso Colombia Haiti Kuwait Pakistan South_Africa Botswana Canada Chile Denmark Ecuador Estonia Finland Honduras Iceland Indonesia Kenya Latvia Lithuania Maldives Mexico Netherlands Norway Peru Poland Qatar Sierra_Leone Sri_Lanka Royal_Dutch_Shell_PLC British_Petroleum_PLC Rio_Tinto_Group_PLC Murmansk_Shipping_Company Greenpeace IPCC EEA WWF)

committee = Committee.find_by(name: 'unep')
unep.each do |country|
  Delegate.create(country: country, committee_id: committee.id)
end

users = %w(ga1 ga3 ga4 ecosoc sc unhcr unep)
users.each do |user|
  User.create(username: user, password: user.to_s + '123', committee_id: Committee.find_by(name: user.to_s).id, is_admin: false)
end
User.create(username: 'devinda', password: '19890618', is_admin: true)
