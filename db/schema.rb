# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160114212203) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "committees", force: :cascade do |t|
    t.string   "name"
    t.boolean  "special",    default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "committees", ["id"], name: "index_committees_on_id", using: :btree
  add_index "committees", ["name"], name: "index_committees_on_name", using: :btree

  create_table "delegates", force: :cascade do |t|
    t.integer  "committee_id"
    t.string   "country"
    t.integer  "speaker_list_score",   default: 0
    t.integer  "speaker_list_count",   default: 1
    t.integer  "poi_score",            default: 0
    t.integer  "poi_count",            default: 1
    t.integer  "ror_score",            default: 0
    t.integer  "ror_count",            default: 1
    t.integer  "ros_score",            default: 0
    t.integer  "ros_count",            default: 1
    t.integer  "mod_structure_score",  default: 0
    t.integer  "mod_structure_count",  default: 1
    t.integer  "mod_speech_score",     default: 0
    t.integer  "mod_speech_count",     default: 1
    t.integer  "lobbying_score",       default: 0
    t.integer  "lobbying_count",       default: 1
    t.integer  "fps",                  default: 0
    t.integer  "res_speech_score",     default: 0
    t.integer  "res_speech_count",     default: 1
    t.integer  "amendments_score",     default: 0
    t.integer  "amendments_count",     default: 1
    t.integer  "other_speeches_score", default: 0
    t.integer  "other_speeches_count", default: 1
    t.boolean  "sponsor",              default: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "delegates", ["country", "committee_id"], name: "index_delegates_on_country_and_committee_id", using: :btree
  add_index "delegates", ["id"], name: "index_delegates_on_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username",         null: false
    t.string   "crypted_password"
    t.string   "salt"
    t.integer  "committee_id"
    t.boolean  "is_admin"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

end
