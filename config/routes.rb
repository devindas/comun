Rails.application.routes.draw do
  get 'admin/index'

  root to: 'pages#index'
  resources :committees, only: [] do
    resource :client, only: [:update] do
      get :index , to: 'clients#index'
    end
    resources :admin, only: [:index, :create, :new]
  end
  resources :sessions, only: [:new, :create, :destroy]
  get '/log_in', to: 'sessions#new'
  delete '/log_out', to: 'sessions#destroy'
end
