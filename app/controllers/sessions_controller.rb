class SessionsController < ApplicationController
  def new
  end

  def create
    if login(params[:username], params[:password])
      if current_user.is_admin?
        flash[:success] = "Welcome back, Admin !"
        redirect_back_or_to root_path
      else
        flash[:success] = "Welcome back, Chair of #{current_user.committee.name.upcase} !"
        redirect_back_or_to committee_client_path(committee_id: current_user.committee_id.to_i)
      end


    else
      flash.now[:warning] = 'Username and/or password is incorrect'
      render 'new'
    end
  end

  def destroy
    logout
    flash[:success] = 'See you !'
    redirect_to log_in_path
  end
end
