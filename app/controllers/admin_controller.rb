class AdminController < ApplicationController
  before_action :require_login
  def index
    @committee_name = Committee.find(params[:committee_id]).name
    unless current_user.is_admin?
      unless params[:committee_id].to_i == current_user.committee_id
        flash[:warning] = 'You cannot access this Committee'
        redirect_to root_path
      end
    end
  end

  def new
  end

  def create
    committee_id = params[:committee_id].to_i
    country = params[:country]
    Delegate.create(country: country, committee_id: committee_id) unless Delegate.find_by(country: country, committee_id: committee_id)
    redirect_to root_path
  end
end
