class ClientsController < ApplicationController
  before_action :require_login
  include ClientHelper
  def index
    @committee_name = Committee.find(params[:committee_id]).name
    # unless current_user.is_admin?
    #   unless params[:committee_id].to_i == current_user.committee_id
    #     flash[:warning] = 'You cannot access this Committee'
    #     redirect_to root_path
    #   end
    # end
    if params[:committee_id].to_i != current_user.committee_id && current_user.is_admin? == false
      flash[:warning] = 'You cannot access this Committee'
      redirect_to root_path
    end


  end

  def update
    delegate_id = params[:delegate_id].to_i
    # puts delegate_id
    committee_id = params[:committee_id].to_i
    # puts committee_id
    score = params[:score].to_i
    # puts score
    @identifier = params[:identifier].to_s
    # puts @identifier

    if @identifier == 'l'
      if score == 1
        score = true
      elsif score == 0
        score = false
      end
    end
    fields = update_identifier(@identifier)
    delegate = Delegate.find_by(id: delegate_id, committee_id: committee_id)
    if @identifier != 'l' && delegate.send("#{fields[0]}").to_i > 0
      delegate.send("#{fields[1]}=", (delegate.send("#{fields[1]}")).to_i + 1) unless fields[1] == ''
    end
    delegate.send("#{fields[0]}=", score)
    delegate.save

    respond_to do |format|
      format.js {}
    end
  end
end
