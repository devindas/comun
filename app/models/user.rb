class User < ActiveRecord::Base
  authenticates_with_sorcery!
  belongs_to :committee

  def is_admin?
    return self.is_admin
  end
end
