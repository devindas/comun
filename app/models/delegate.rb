class Delegate < ActiveRecord::Base
  belongs_to :committee
  default_scope { order('country ASC') }
end
