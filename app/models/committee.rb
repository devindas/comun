class Committee < ActiveRecord::Base
  has_many :delegates
  has_many :users
end
