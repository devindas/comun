module AdminHelper
  def score(id)
    delegates = Committee.find(id).delegates
    scores = []
    delegates.each do |delegate|
      speaker_list = (delegate.speaker_list_score.to_f / delegate.speaker_list_count.to_f).round(1)
      poi = (delegate.poi_score.to_f / delegate.poi_count.to_f).round(1)
      ror = (delegate.ror_score.to_f / delegate.ror_count.to_f).round(1)
      ros = (delegate.ros_score.to_f / delegate.ros_count.to_f).round(1)
      mod_structure = (delegate.mod_structure_score.to_f / delegate.mod_structure_count.to_f).round(1)
      mod_speech = (delegate.mod_speech_score.to_f / delegate.mod_speech_count.to_f).round(1)
      lobbying = (delegate.lobbying_score.to_f / delegate.lobbying_count.to_f).round(1)
      fps = (delegate.fps.to_f).round(1)
      res_speech = (delegate.res_speech_score.to_f / delegate.res_speech_count.to_f).round(1)
      amendments = (delegate.amendments_score.to_f / delegate.amendments_count.to_f).round(1)
      other_speeches = (delegate.other_speeches_score / delegate.other_speeches_count.to_f).round(1)
      final_score = speaker_list + poi + ror + ros + mod_structure + mod_speech + lobbying + fps + res_speech + amendments + other_speeches
      scores.push([delegate.country, speaker_list, poi, ror, ros, mod_structure, mod_speech, lobbying, fps, res_speech, amendments, other_speeches, final_score, delegate.speaker_list_score , delegate.speaker_list_count , delegate.poi_score , delegate.poi_count , delegate.ror_score , delegate.ror_count , delegate.ros_score , delegate.ros_count , delegate.mod_structure_score , delegate.mod_structure_count , delegate.mod_speech_score , delegate.mod_speech_count , delegate.lobbying_score , delegate.lobbying_count , delegate.res_speech_score , delegate.res_speech_count , delegate.amendments_score , delegate.amendments_count , delegate.other_speeches_score,  delegate.other_speeches_count])
    end
    scores
  end
end
