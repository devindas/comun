module ClientHelper
  def isSpecial?(id)
    if Committee.find(id).special
      return true
    else
      return false
    end
  end

  def get_country_list(id)
    delegates = Committee.find(id).delegates
    country = {}
    delegates.each do |delegate|
      country[delegate.id] = delegate.country
    end
    country
  end

  def update_identifier(id)
    fields = []

    case id
    when 'a'
      fields[0] = 'speaker_list_score'
      fields[1] = 'speaker_list_count'
    when 'b'
      fields[0] = 'poi_score'
      fields[1] = 'poi_count'
    when 'c'
      fields[0] = 'ror_score'
      fields[1] = 'ror_count'
    when 'd'
      fields[0] = 'ros_score'
      fields[1] = 'ros_count'
    when 'e'
      fields[0] = 'mod_structure_score'
      fields[1] = 'mod_structure_count'
    when 'f'
      fields[0] = 'mod_speech_score'
      fields[1] = 'mod_speech_count'
    when 'g'
      fields[0] = 'lobbying_score'
      fields[1] = 'lobbying_count'
    when 'h'
      fields[0] = 'fps'
      fields[1] = ''
    when 'i'
      fields[0] = 'res_speech_score'
      fields[1] = 'res_speech_count'
    when 'j'
      fields[0] = 'amendments_score'
      fields[1] = 'amendments_count'
    when 'k'
      fields[0] = 'other_speeches_score'
      fields[1] = 'other_speeches_count'
    when 'l'
      fields[0] = 'sponsor'
      fields[1] = ''
    end

    return fields
  end
end
